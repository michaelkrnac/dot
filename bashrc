# .bashrc by Michael Krnac

#################
# Colors
#################

# enable 256 colors on xterm 
export TERM='xterm-256color'

# set colors with base16 script
BASE16_SHELL=$HOME/.config/base16-shell/
[ -n "$PS1" ] && [ -s $BASE16_SHELL/profile_helper.sh ] && eval "$($BASE16_SHELL/profile_helper.sh)"


#################
# ls
#################
# ls config
alias ls='ls -hF --color=tty'                 # classify files in colour
alias ll='ls -l'                              # long list
alias la='ls -lA'                              # long list




#################
# Default bins
#################
export EDITOR='vim'

export PS1='\[\033[0;32;40m\]\u\[\033[0;34m\]@\h\[\033[0;35m\]:\w \[\033[0m\]$ '
PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
