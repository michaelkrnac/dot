" --------------------------
" DEFAULTS
" --------------------------
"force vim to use vim mode and not vi mode 
set nocompatible        

" let backspace work as usual
set backspace=indent,eol,start

" cursors setting in normal mode block in insert mode line
let &t_ti.="\e[1 q"
let &t_SI.="\e[5 q"
let &t_EI.="\e[1 q"
let &t_te.="\e[0 q"

" show tabs 4 spaces wide
set tabstop=4
set noexpandtab
set softtabstop=4
set shiftwidth=4

" enable autoindent based on filetype
filetype plugin indent on 
set autoindent

" --------------------------
" Design
" --------------------------
" set 256 colors
set t_Co=256

" set color scheme
colorscheme desert

" enable syntax highlithing
syntax on

" enable line numbers
set number

" enable utf8
set encoding=utf8

" set font
set guifont=DejaVu\ Sans\ Mono

" toggle menu and toolbar in gvim
set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar

" folding
"set foldmethod=indent   
"set foldnestmax=10
"set nofoldenable
"set foldlevel=2

" intend long lines
set breakindent showbreak=...

" -------------------------
" KEYBOARD
" -------------------------

" enable mouse
set mouse=a

nnoremap <Leader>sb iMichael Krnac (mkrnac@de.pepperl-fuchs.com)<Esc>
nnoremap <Leader>sp iMichael Krnac (michael@krnac.de)<Esc>
