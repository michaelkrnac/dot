#!/bin/bash
echo "Post install script of Michael Krnac (after Debian installation)"

# change to home folder
cd

#################
# GOLANG
#################
wget https://storage.googleapis.com/golang/go1.9.2.linux-amd64.tar.gz -P ~/downloads
sudo tar -C /usr/local -xzf ~/downloads/go1.9.2.linux-amd64.tar.gz
echo 'export PATH=$PATH:/usr/local/go/bin' >> .profile
mkdir go
